# -*- coding: utf-8 -*-

"""
    Administração de Fornecedores.
"""

from django.contrib import admin

from fornecedores.models import TipoFornecedor, Fornecedor, \
                                CondicoesPagamento, DiaFuncionamento, \
                                FuncionamentoFornecedor, CondicoesEntrega


class TipoFornecedorAdmin(admin.ModelAdmin):
    """
        Administração do Tipo de Fornecedor.
    """
    pass
admin.site.register(TipoFornecedor, TipoFornecedorAdmin)


class CondicoesEntregaInline(admin.TabularInline):
    """
        Administração do Condicoes de Entrega Inline.
    """
    model = CondicoesEntrega


class FuncionamentoFornecedorInline(admin.TabularInline):
    """
        Administração do FuncionamentoFornecedor Inline.
    """
    model = FuncionamentoFornecedor


class FornecedorAdmin(admin.ModelAdmin):
    """
        Administração do Fornecedor.
    """
    inlines = [FuncionamentoFornecedorInline, CondicoesEntregaInline]
    list_display = ('nome', 'tipo', 'cidade_bairro', 'telefone')
    list_filter = ['tipo']
    search_fields = ('nome', 'tipo', 'cidade_bairro')
admin.site.register(Fornecedor, FornecedorAdmin)


class CondicoesPagamentoAdmin(admin.ModelAdmin):
    """
        Administração das CondicoesPagamento.
    """
    pass
admin.site.register(CondicoesPagamento, CondicoesPagamentoAdmin)


class CondicoesEntregaAdmin(admin.ModelAdmin):
    """
        Administração das CondicoesEntrega.
    """
    list_display = ('condicao', 'preco')
    search_fields = ('condicao',)
admin.site.register(CondicoesEntrega, CondicoesEntregaAdmin)


class DiaFuncionamentoAdmin(admin.ModelAdmin):
    """
        Administração dos DiaFuncionamento.
    """
    pass
admin.site.register(DiaFuncionamento, DiaFuncionamentoAdmin)
