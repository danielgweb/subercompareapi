# -*- coding: utf-8 -*-

"""
    Modelo de Fornecedores
"""

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from administracao.models import CidadeBairro


class TipoFornecedor(models.Model):
    tipo = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Tipo de fornecedor')
        verbose_name_plural = _('Tipo de fornecedores')

    def __unicode__(self):
        return self.tipo


class Fornecedor(models.Model):
    nome = models.CharField(max_length=255)
    tipo = models.ForeignKey('TipoFornecedor')
    endereco = models.CharField(max_length=255)
    cidade_bairro = models.ForeignKey(CidadeBairro,
                                      verbose_name=_(u'Cidade e bairro'))
    telefone = models.CharField(max_length=255)
    site = models.URLField(max_length=255, blank=True)
    email = models.EmailField(blank=True)
    condicoes_pagamento = models.ManyToManyField('CondicoesPagamento')
    coordenadas_gps = models.CharField(max_length=255)
    descricao = models.TextField(blank=True)

    class Meta:
        verbose_name = _('Fornecedor')
        verbose_name_plural = _('Fornecedores')

    def __unicode__(self):
        return self.nome


class CondicoesPagamento(models.Model):
    condicao = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Condição de pagamento')
        verbose_name_plural = _('Condições de pagamento')

    def __unicode__(self):
        return self.condicao


class CondicoesEntrega(models.Model):
    fornecedor = models.ForeignKey('Fornecedor', on_delete=models.CASCADE)
    condicao = models.CharField(max_length=255)
    preco = models.FloatField()

    class Meta:
        verbose_name = _('Condição de entrega')
        verbose_name_plural = _('Condições de entrega')

    def __unicode__(self):
        return self.condicao + ' - Preço R$: ' + str(self.preco)


class DiaFuncionamento(models.Model):
    dia = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Dia de funcionamento')
        verbose_name_plural = _('Dias de funcionamento')

    def __unicode__(self):
        return self.dia


class FuncionamentoFornecedor(models.Model):
    fornecedor = models.ForeignKey('Fornecedor', on_delete=models.CASCADE)
    dia = models.ForeignKey('DiaFuncionamento')
    horas24 = models.BooleanField(verbose_name=_(u'24 horas'))
    horario_abertura = models.TimeField(blank=True, null=True, auto_now=False,
                                        auto_now_add=False)
    horario_fechamento = models.TimeField(blank=True, null=True,
                                          auto_now=False, auto_now_add=False)

    class Meta:
        verbose_name = _('Funcionamento do fornecedor')
        verbose_name_plural = _('Funcionamento do fornecedor')

    def __unicode__(self):
        return self.dia.dia
