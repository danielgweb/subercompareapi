# -*- coding: utf-8 -*-

"""
    Configuração do App Fornecedores
"""

from __future__ import unicode_literals

from django.apps import AppConfig


class FornecedoresConfig(AppConfig):
    """
        Configuração do App Fornecedores
    """
    name = 'fornecedores'
