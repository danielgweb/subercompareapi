# -*- coding: utf-8 -*-

"""
    Administração geral.
"""

from django.contrib import admin

from administracao.models import Cidade, CidadeBairro, Coletador


class CidadeAdmin(admin.ModelAdmin):
    """
        Administração de cidades.
    """
    pass
admin.site.register(Cidade, CidadeAdmin)


class CidadeBairroAdmin(admin.ModelAdmin):
    """
        Administração de bairros.
    """
    list_display = ('cidade', 'bairro')
    list_filter = ['cidade__nome', 'bairro']
    search_fields = ('cidade__nome', 'bairro')
admin.site.register(CidadeBairro, CidadeBairroAdmin)


class ColetadorAdmin(admin.ModelAdmin):
    """
        Administração de coletadores.
    """
    pass
admin.site.register(Coletador, ColetadorAdmin)
