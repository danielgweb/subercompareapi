# -*- coding: utf-8 -*-

"""
    Configuração do App Administração
"""

from __future__ import unicode_literals

from django.apps import AppConfig


class AdministracaoConfig(AppConfig):
    """
        Configuração do App Administração
    """
    name = 'administracao'
