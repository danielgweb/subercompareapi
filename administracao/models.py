# -*- coding: utf-8 -*-

"""
    Modelo de Componentes de Administração.
"""

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models


class Cidade(models.Model):
    """
        Cidade Model.
    """
    nome = models.CharField(max_length=255)

    class Meta:
        """
            Cidade Model descrição;
        """
        verbose_name = _('Cidade')
        verbose_name_plural = _('Cidades')

    def __unicode__(self):
        return self.nome


class CidadeBairro(models.Model):
    """
        CidadeBairro Model.
    """
    cidade = models.ForeignKey('Cidade')
    bairro = models.CharField(max_length=255, blank=True)

    class Meta:
        """
            CidadeBairro Model descrição.
        """
        verbose_name = _('Cidade e bairro')
        verbose_name_plural = _('Cidades e bairros')

    def __unicode__(self):
        if self.bairro != '':
            return self.cidade.nome + ' - ' + self.bairro
        else:
            return self.cidade.nome


class Coletador(models.Model):
    """
        Coletador Model.
    """
    nome = models.CharField(max_length=255)

    class Meta:
        """
            Coletador Model descrição.
        """
        verbose_name = _('Coletador')
        verbose_name_plural = _('Coletadores')

    def __unicode__(self):
        return self.nome
