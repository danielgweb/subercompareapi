from rest_framework import serializers
from produtos.models import Cotacoes


class CotacoesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cotacoes
        fields = ('produto_cadastrado',)
