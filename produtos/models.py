# -*- coding: utf-8 -*-

"""
    Modelo de produtos.
"""

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from administracao.models import Coletador
from fornecedores.models import Fornecedor


class ProdutoCadastrado(models.Model):
    """
        Modelo do Produto Cadastrado
    """
    codigo = models.CharField(max_length=255)
    codigo_barras = models.CharField(verbose_name=_(u'código de barras'),
                                     max_length=255)
    nome = models.CharField(max_length=255)
    volume = models.FloatField(blank=True)
    unidade_do_volume = models.ForeignKey('UnidadeVolume', blank=True)
    unidades = models.IntegerField(default=1)
    volume_total = models.FloatField(editable=False, blank=True, null=True)
    categoria_do_produto = models.ForeignKey('CategoriaProduto')
    descricao = models.TextField(blank=True)

    class Meta:
        """
            Descrição do modelo Produto Cadastrado
        """
        verbose_name = _('Produto cadastrado')
        verbose_name_plural = _('Produtos cadastrados')

    def __unicode__(self):
        return str(self.codigo) + ': ' + self.nome + ' ' + str(self.volume) \
                + ' ' + str(self.unidade_do_volume)


class CategoriaProduto(models.Model):
    """
        Modelo de Categorias do Produto Cadastrado
    """
    nome = models.CharField(max_length=255)

    class Meta:
        """
            Descrição de Categorias do Produto Cadastrado
        """
        verbose_name = _('Categoria do produto')
        verbose_name_plural = _('Categorias dos produtos')

    def __unicode__(self):
        return self.nome


class UnidadeVolume(models.Model):
    """
        Modelo da Unidade de Volume
    """
    unidade = models.CharField(max_length=255)

    class Meta:
        """
            Descrição do modelo UnidadeVolume
        """
        verbose_name = _('Unidade do volume')
        verbose_name_plural = _('Unidades dos volumes')

    def __unicode__(self):
        return self.unidade

    def __str__(self):
        return self.unidade


class Cotacoes(models.Model):
    """
        Modelo de Cotações
    """
    produto_cadastrado = models.ForeignKey('ProdutoCadastrado')
    fornecedor = models.ForeignKey(Fornecedor)
    preco = models.FloatField()
    promocional = models.BooleanField()
    coletador = models.ForeignKey(Coletador)
    data_hora_coleta = models.DateTimeField(auto_now=True)

    class Meta:
        """
            Descrição do Modelo de Cotações
        """
        verbose_name = _('Cotação')
        verbose_name_plural = _('Cotações')

    def __unicode__(self):
        return self.produto_cadastrado.nome
