# -*- coding: utf-8 -*-

"""
    Administração de produtos.
"""

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from produtos.models import ProdutoCadastrado, CategoriaProduto, \
                            UnidadeVolume, Cotacoes


class ProdutoCadastradoAdmin(admin.ModelAdmin):
    """
        Administração de produtos cadastrados.
    """
    list_display = ('codigo', 'unidades', 'nome', 'volume_view',
                    'volume_total_view', 'categoria_do_produto',
                    'codigo_barras')
    list_filter = ['categoria_do_produto__nome']
    search_fields = ('codigo', 'nome', 'descricao',
                     'categoria_do_produto__nome', 'volume', 'codigo_barras')
    ordering = ('codigo',)

    def volume_view(self, obj):
        """
            Retorna o volume com a unidade, retirando o 0 após o ponto.
        """
        return str(obj.volume).rstrip('0').rstrip('.') + ' ' \
            + str(obj.unidade_do_volume)

    def volume_total_view(self, obj):
        """
            Retorna o volume total, alterando a unidade caso exceda 1000.
        """
        volumetotal = obj.volume_total
        unidade = str(obj.unidade_do_volume)
        if volumetotal > 1000:
            volumetotal = float("%.2f" % (volumetotal/1000))
            if unidade.lower() == 'mg':
                unidade = 'kg'
            if unidade.lower() == 'ml':
                unidade = 'L'
        return str(volumetotal).rstrip('0').rstrip('.') + ' ' + unidade

    def save_model(self, request, obj, form, change):
        obj.volume_total = obj.volume * obj.unidades
        obj.save()

    volume_view.short_description = _(u'Volume')
    volume_total_view.short_description = _(u'Volume Total')

admin.site.register(ProdutoCadastrado, ProdutoCadastradoAdmin)


class CategoriaProdutoAdmin(admin.ModelAdmin):
    """
        Administração de categorias dos produtos cadastrados.
    """
    pass
admin.site.register(CategoriaProduto, CategoriaProdutoAdmin)


class UnidadeVolumeAdmin(admin.ModelAdmin):
    """
        Administração das unidades de volume.
    """
    pass
admin.site.register(UnidadeVolume, UnidadeVolumeAdmin)


class CotacoesAdmin(admin.ModelAdmin):
    """
        Administração das cotações.
    """
    list_display = ('produto_cadastrado', 'fornecedor', 'preco',
                    'promocional', 'coletador', 'data_hora_coleta')
    list_filter = ['promocional', 'fornecedor', 'coletador']
    search_fields = ('produto_cadastrado__nome', 'fornecedor__nome',
                     'coletador__nome')
admin.site.register(Cotacoes, CotacoesAdmin)
