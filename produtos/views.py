# -*- coding: utf-8 -*-

"""
    View do App Produtos
"""

from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.authentication import TokenAuthentication
# from rest_framework.permissions import IsAdminUser

from produtos.models import Cotacoes

class FornecedorView(APIView):

    def get(self, request, format=None):

        fornecedores_sql = Cotacoes.objects.raw(
            'SELECT s2.nome, s2.id, COUNT(DISTINCT produto_cadastrado_id) '
            ' as total FROM produtos_cotacoes s1 JOIN fornecedores_fornecedor '
            's2 ON (s1.fornecedor_id = s2.id) GROUP BY s2.nome, s2.id '
            'ORDER BY total DESC'
            )

        fornecedores = []
        for fornecedor in fornecedores_sql:
            fornecedores.append({
                                "id": fornecedor.id,
                                "nome": fornecedor.nome,
                            })

        return Response(fornecedores)


class CotacaoView(APIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAdminUser,)

    def get(self, request, format=None):

        # Obtem os produtos
        produtos_sql = Cotacoes.objects.raw(
            'SELECT DISTINCT(produto_cadastrado_id), s2.id, s2.nome, s2.codigo '
            'FROM produtos_cotacoes s1 JOIN produtos_produtocadastrado s2 '
            'ON (s1.produto_cadastrado_id = s2.id)'
            )

        produtos = []
        for produto in produtos_sql:
            produtos.append({
                                "id": produto.id,
                                "codigo": produto.codigo,
                                "nome": produto.nome,
                                "precos": [],
                            })

        # Obtem as cotacoes dos produtos
        cotacoes_sql = Cotacoes.objects.raw(
            'SELECT DISTINCT ON (fornecedor_id, produto_cadastrado_id) '
            'fornecedor_id, produto_cadastrado_id, id, preco, promocional '
            'FROM produtos_cotacoes ORDER BY fornecedor_id, '
            'produto_cadastrado_id, data_hora_coleta DESC, id'
            )

        cotacoes = cotacoes_sql

        # Atribui as cotacoes a cada produto
        for produto in produtos:
            for cotacao in cotacoes:
                if produto['id'] == cotacao.produto_cadastrado_id:
                    produto['precos'].append(
                        {cotacao.fornecedor_id: cotacao.preco}
                        )
                    del cotacao
            produto.pop('id')

        return Response(produtos)
