# -*- coding: utf-8 -*-

"""
    Filtro de preços das cotações
"""

from django import template

register = template.Library()


@register.filter
def obter_preco(cotacoes, fornecedor):
    """
        Retorna o preço por fornecedor ou '-' caso não tenha preço.
    """
    preco = str(cotacoes.get(str(fornecedor), '-'))
    return preco if preco == '-' else 'R$ ' + preco
