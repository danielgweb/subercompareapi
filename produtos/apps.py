# -*- coding: utf-8 -*-

"""
    Configuração do App Produtos
"""

from __future__ import unicode_literals

from django.apps import AppConfig


class ProdutoConfig(AppConfig):
    """
        Configuração do App Produto
    """
    name = 'produto'
